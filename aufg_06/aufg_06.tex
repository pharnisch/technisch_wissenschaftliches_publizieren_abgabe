% Poster

% Option posterdraft für DinA4-Test-Druck
\documentclass[a0, portrait]{a0poster} % alternativ: beamerposter
\usepackage{lipsum}
\usepackage[ngerman]{babel}
\usepackage{booktabs}
\usepackage{multicol}
\usepackage{pstricks}
\usepackage{float}
%\usepackage{cleveref}
\usepackage[ngerman]{struktex}
\usepackage{listings} % Quellcode einbinden und formatiert darstellen (Syntax-Highlighting)
\lstset{ % Setzen der Programmiersprache für Paket listings
	language=Python
}

\usepackage[ % Verwaltung und Gestaltung der Literaturliste, Verwaltung der Bibliographie durch biber und biblatex
backend=biber, 
autolang=hyphen, % Trennung gemäß der mit babel gesetzten Sprache
style=alphabetic, % Verweise ähnlich zu alpha.bst: XXX00
citestyle=alphabetic, % mehrere Titel eines Autors werden XXX00a, ... zitiert
giveninits=false % Vornamen werden nicht abgekürzt
]{biblatex}
\defbibheading{bibliography}{} % es wird kein Code für eine Überscrift des Literaturverzeichnisses angegeben, die Überschrift wird anderweitig erzeugt
\addbibresource{../common/thesis.bib} % Hinzufuegen der Literatur-Datenbank (auch mehrere möglich)
%\bibliography{../common/thesis} % Alternative Zuweisung der Literatur-Datenbank (.bib gehört nicht dahinter)
%\bibliographystyle{plain} % Stil der Bibliographie
\usepackage[babel,german=quotes]{csquotes} % Titel werden in deutsche Gänsefüßchen gesetzt

\author{Philipp L. Harnisch}
\title{\input{../common/contents/titel-name}}
\setlength\columnsep{60pt}
\setlength{\columnseprule}{0.4pt}

\begin{document}
	\newrgbcolor{MainColor}{.0 .0 0.2}
	\newrgbcolor{SecondaryColor}{.1 .1 .1}
	
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%%%%%%%%%%%%% Kopf %%%%%%%%%%%%%
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	\vspace{1cm}
	\centerline{\MainColor \veryHuge Regularisierung schlecht konditionierter Probleme} 
	\vspace{2cm}
	\centerline{\MainColor \LARGE Philipp Lars Harnisch} 
	\vspace{6cm}
	
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%%%%%%%%%%%%% Rumpf %%%%%%%%%%%%%
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	\begin{multicols}{3} % * for unbalanced columns; without for balanced columns
		\Large % ca 36 pt. für den gesamten Rumpf
		\SecondaryColor % für den Rumpf andere Farbe als für Kopf und Fuß
		
		\section{Einleitender Teil zur Numerik}
		\label{sec:einleitender-teil}
		
		\subsection{Numerisches Lösen von Problemen}
		\label{subsec:num-loesen-von-probl}
		
		Die Numerik befasst sich mit dem Durchdringen von mathematischen Problemen und dem Übertragen dieser in ein Modell, welches sich mithilfe eines Computers lösen lässt.
		Dabei stößt	man in der heutigen Zeit beispielsweise auf die Grenzen der Modellmöglichkeiten bezüglich der Nachkommastellen einer Dezimalzahl.
		Durch diese Ungenauigkeit können Fehler im Modell entstehen, die das Ergebnis vom exakten Ergebnis abweichen lassen. 
		Diese Fehler wiederum pflanzen sich gegebenenfalls fort und verschlimmern sich somit.
		Ein weiterer Grund für Fehler beim Berechnen der Lösung sind abweichende Eingabewerte (beispielsweise von ungenauen Messungen).
		Ein Indikator in der Numerik, der beschreibt, wie stark das Problem auf Eingabefehler reagiert, ist die Kondition.
		
		\subsection{Schwierigkeiten beim numerischen Lösen von linearen Gleichungssystemen}
		\label{subsec:schwierigkeiten}
		
		Lineare Gleichungssysteme (Lineare Gleichungssysteme haben die Form $A x = b$, $A \in \mathcal R^{m \times n}$, $x$, $b \in \mathcal R^{n}$, $m$, $n \in \mathcal N$) mit Matrizen, welche eine schlechte Kondition aufweisen, liefern keine zufrieden stellenden Ergebnisse, wenn man sie mit einfachen numerischen Methoden (LU-Zerlegung oder Householder-Verfahren) lösen möchte. Dies wird in \autocite{num-1} bestätigt:
		\begin{figure}[H]
			
			\centering
			
			\caption[Simple Verfahren bei schlechter Kondition]{LU-Zerlegung und Householder-Verfahren bei schlechter Kondition (mit Störung  $\delta$ und $\|\delta\|_2 = {10}^{-6}$)}
			
			\begin{tabular}{*{3}{c}}
				
				\toprule
				exakt ungestört	& LU, Spalten-Pivot & Householder \\
				
				\midrule
				
				1 & 1.0104e1 & 3.5021e2 \\
				1 & -8.9708e2 & -4.4339e4 \\
				1 & 2.2261e4 & 1.3469e6 \\
				1 & -2.4066e5 & -1.6494e7 \\
				1 & 1.4028e6 & 9.1563e7 \\
				1 & -4.8893e6 & -1.4746e8 \\
				1 & 1.0733e7 & -9.6850e8 \\
				1 & -1.5127e7 & 6.7281e9 \\
				1 & 1.3561e7 & -2.0302e10 \\
				1 & -7.3813e6 & 3.6709e10 \\
				1 & 2.1458e6 & -4.2460e10 \\
				1 & -1.7897e5 & 3.1367e10 \\
				1 & -5.7412e4 & -1.4057e10 \\
				1 & 4.7274e3 & 3.3515e9 \\
				1 & 6.5344e3 & -2.9622e8\\
				
				\bottomrule
				
			\end{tabular}
			
			\label{fig:schlechte-kond}
			
		\end{figure}
		
		\subsection{Frage nach Lösungsstrategien für Problematik schlecht konditionierter Matrizen}
		\label{subsec:frage-nach-loes}
		
		In der folgenden Arbeit wird sich damit beschäftigt, Lösungen für diese Problematik zu erläutern und zu beurteilen.
		
		
		\section{Vergleich zweier Verfahren}
		
		Die folgenden zwei Verfahren besitzen beide einen freien Parameter $\alpha > 0$. Dieser wird sich dadurch auszeichnen, dass bei kleinem $\alpha$ besser approximiert, aber schlechter stabilisiert wird als bei größerem $\alpha$, welches zu einer schlechten Approximation, aber einer guten Stabilisierung führt.
		
		Die beiden Verfahren, die verglichen werden, sind:
		\begin{enumerate}
			\item Truncated Singular Value Decomposition
			\item Tikhonov-Regularisierung
		\end{enumerate}
		
		Die Wahl des $\alpha$ ist ein entscheidender Punkt, da ein zu kleines $\alpha$ die Kondition gegebenenfalls nicht zufriedenstellend verbessert und ein zu großes $\alpha$ die Matrix $A_\alpha$ zu sehr von $A$ abweichen lässt. Im Rahmen dieser Arbeit wird jedoch nicht näher auf diese Parameterwahl eingegangen.
		
		\subsection{Truncated Singular Value Decomposition}
		
		Die Idee der TSVD (Truncated Singular Value Decomposition) ist es, die Konditionszahl möglichst klein zu halten, indem man alle Singulärwerte $\sigma_i$ -- beginnend bei $\sigma_r$ (der kleinsten) -- aus $\Sigma$ entfernt, welche zu einer zu großen Konditionszahl führen.
		
		Dabei entsteht aus dem $A$ das $A_\alpha$, welches wie folgt mit einem $\alpha > 0$ definiert wird:
		\[ A_\alpha = U \Sigma_\alpha V^T  \]
		wobei
		\[ \Sigma_\alpha = \left( 
		\begin{array}{*{3}{c}|*{3}{c}}
		\sigma_1 & & & & \vdots & \\
		& \ddots & & \cdots & 0 & \cdots \\
		& & \sigma_k & & \vdots & \\
		\cline{1-6}
		& \vdots & & & \vdots & \\
		\cdots & 0 & \cdots & \cdots & 0 & \cdots \\
		& \vdots & & & \vdots & \\
		\end{array}
		\right) \]
		und
		\[ \frac{\sigma_1}{\sigma_k} \le \frac{1}{\alpha},\,\frac{\sigma_1}{\sigma_k+1} \ge \frac{1}{\alpha} \]
		
		Es muss im Programmcode bei einer numerischen Umsetzung, also im ersten Schritt, die Singulärwertzerlegung erfolgen, um im Anschluss die oben stehende Bedingung für diese zu prüfen und bei Nichtbestehen der Bedingung sie mit 0 zu ersetzen.
		
		Gelöst wird das System im letzten Zuge mit dem Berechnen von $x_\alpha = A_\alpha^+ b$, wobei $A_\alpha^+$ die Pseudo-Inverse von $A_\alpha$ ist.
		
		\subsection{Tikhonov-Regularisierung}
		
		Bei der Tikhonov-Regularisierung wird für $\alpha > 0$ die Funktion
		\[ J_\alpha(x) = \|Ax-b\|_2^2 + \alpha^2 \|x\|_2^2 \]
		betrachtet. 
		
		Diese wird, wie in \cite{num-1} beschrieben, umgeformt und führt zu der Normalgleichung für $ \|Ax-b\|_2 \rightarrow min$, ergänzt um einen Term $ \alpha^2 I $, welcher gewährleistet, dass die Systemmatrix $ A^T A + \alpha^2 I $ symmetrisch positiv definit (Symmetrisch positiv definite Matrizen sind symmetrisch und all ihre Untermatrizen sind positiv.) ist:
		\[ ( A^T A + \alpha^2 I ) x_\alpha = A^T b \]
		
		Damit ist für $x = A^+ b$ zum Parameter $\alpha$ die Tikhonov-Regularisierung für $A \in \mathcal R^{m \times n}$, $\alpha > 0$:
		\[ x_\alpha = ( A^T A + \alpha^2 I )^{-1} A^T b \]
		
		Nach Satz 234 aus \cite{num-1} konvergiert die Tikhonov-Regularisierung für $\alpha \rightarrow 0$ gegen die exakte Lösung $x$.

		\subsection{Programm}
		
		Im Code-Ausschnitt sieht man, wie die beiden Verfahren mit Hilfe der python-Bibliothek numpy umgesetzt werden.
		
		Diese Bibliothek wird wiefolgt in python eingebunden:
		
		\begin{verbatim}
		import numpy as np
		\end{verbatim}
		
		\begin{figure}[H]
			
			\caption[Code-Ausschnitt]{TSVD und Tikhonov in der Praxis}
			
			\lstinputlisting[stepnumber=5, firstnumber=30, linerange=40-100]{../common/app.py}
			
			\label{fig:code}
			
		\end{figure}
		
		\section{Ergebnisse}
		
		Bei den vorgestellten Regularisierungsmethoden sieht man, dass, bei gewähltem Parameter $\alpha = 10^{-5}$, das TSVD-Verfahren das eindeutig bessere Ergebnis liefert. Die Regularisierung von Tikhonov ist dabei nicht zufriedenstellend. Tikhonov liefert dennoch bessere Ergebnisse, als die LU-Zerlegung und das Householder-Verfahren.
		
		\begin{figure}[H]
			
			\centering
			
			\caption[Bessere Verfahren bei schlechter Kondition]{TSVD und Tikhonov-Regularisierung mit $\alpha = {10}^{-6}$ bei schlechter Kondition (mit Störung  $\delta$ und $\|\delta\|_2 = {10}^{-6}$)}
			
			\begin{tabular}{*{3}{c}}
				
				\toprule
				
				exakt ungestört & TSVD & Tikonov \\
				
				\midrule
				
				1 & 1.00007119 & 0.0005313 \\
				1 & 0.99927293 & -0.01131297 \\
				1 & 1.00093967 & 0.05203292 \\
				1 & 1.00146821 & -0.06122135 \\
				1 & 0.99964594 & -0.02710865 \\
				1 & 0.99822562 & 0.02300388 \\
				1 & 0.99808526 & 0.04081066 \\
				1 & 0.99896997 & 0.02984065 \\
				1 & 1.00029859 & 0.0059139 \\
				1 & 1.00152332 & -0.01742649 \\
				1 & 1.00223858 & -0.03172441 \\
				1 & 1.00218661 & -0.03297897 \\
				1 & 1.00122982 & -0.02012903 \\
				1 & 0.9993181 & 0.00611718 \\
				1 & 0.99646051 & 0.04415 \\
				
				\bottomrule
				
			\end{tabular}
			
			\label{fig:schlechte-kond-bessere-verfahren}
			
		\end{figure}
	
		\section{Literatur}
		\label{sec:literatur}
		
		\printbibliography

	\end{multicols}

	\vfill % fuellt den Raum zwischen dem Element hierueber und hierunter, bis die gesamte Seite ausgefuellt ist

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%%%%%%%%%%%%% Fuß %%%%%%%%%%%%%
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	\vspace{5cm}
	
	\MainColor %\LARGE
	
	\begin{tabular}{lcr}
		
		\begin{tabular}{*{1}{l}}
			Philipp Lars Harnisch \\
			Dresdener Straße 76 \\
			52525 Heinsberg \\
			\\
			E-Mail: p.harnisch.privat@gmail.com \\
		\end{tabular}
		&
		
		\hspace{44cm}
		&
		
		\begin{tabular}{*{1}{l}}
			\\
			\\
			\\
			\\
			https://www.fiktive-quelle.org/originale-arbeit.pdf \\
		\end{tabular}
		\\
		
	\end{tabular}
	


	
	
\end{document}

