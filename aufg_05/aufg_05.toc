\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {ngerman}{}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Einleitender Teil zur Numerik}{1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1}Numerisches Lösen von Problemen}{1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.2}Schwierigkeiten beim numerischen Lösen von linearen Gleichungssystemen}{2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.3}Frage nach Lösungsstrategien für Problematik schlecht konditionierter Matrizen}{3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}Grundlagen zur weiteren Arbeit}{3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1}Pseudo-Inverse}{3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2}Singulärwerte}{3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.2.1}Definition von Singulärwerten}{3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.2.2}Singulärwertzerlegung}{3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.2.3}Lösungsansatz mit Singulärwerten}{4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3}Vergleich zweier Verfahren}{4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1}Truncated Singular Value Decomposition}{4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2}Tikhonov-Regularisierung}{5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3}Programm}{5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4}Ergebnisse}{6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5}Weitere Schritte}{6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\nonumberline Literatur}{7}
