\chapter{Vergleich zweier Verfahren}
\label{chap:vergl-zweier-verf}

	\ifpbeamer
		\begin{frame}{TSVD und Tikhonov-Regularisierung}
			\begin{itemize}[<+->]
				\item Freier Parameter $\alpha > 0$ (Es wird nicht näher auf Parameterwahl eingegangen)
				\item Bei kleinem $\alpha$ wird besser approximiert, schlechter stabilisiert
				\item Bei großem $\alpha$ wird schlechter approximiert, besser stabilisiert
			\end{itemize}
		\end{frame}
	\else
		Die folgenden zwei Verfahren besitzen beide einen freien Parameter $\alpha > 0$. Dieser wird sich dadurch auszeichnen, dass bei kleinem $\alpha$ besser approximiert, aber schlechter stabilisiert wird als bei größerem $\alpha$, welches zu einer schlechten Approximation, aber einer guten Stabilisierung führt.
		
		Die beiden Verfahren, die verglichen werden, sind:
		\begin{enumerate}
			\item Truncated Singular Value Decomposition
			\item Tikhonov-Regularisierung
		\end{enumerate}
		
		Die Wahl des $\alpha$ ist ein entscheidender Punkt, da ein zu kleines $\alpha$ die Kondition gegebenenfalls nicht zufriedenstellend verbessert und ein zu großes $\alpha$ die Matrix $A_\alpha$ zu sehr von $A$ abweichen lässt. Im Rahmen dieser Arbeit wird jedoch nicht näher auf diese Parameterwahl eingegangen.
	\fi

	\section{Truncated Singular Value Decomposition}
	\label{sec:tsvd}
	
		\ifpbeamer
			\begin{frame}{TSVD}
				\begin{itemize}[<+->]
					\item Idee: Konditionszahl möglichst klein halten
					\item Umsetzen: Singulärwerte $\sigma_i$ -- beginnend bei $\sigma_r$ (der kleinsten) -- aus $\Sigma$ entfernen
					\item Dabei entsteht $ A_\alpha = U \Sigma_\alpha V^T $ mit $ \frac{\sigma_1}{\sigma_k} \le \frac{1}{\alpha},\,\frac{\sigma_1}{\sigma_k+1} \ge \frac{1}{\alpha} $
					\item Näherung ist dann $ x_\alpha = A_\alpha^+ b$, wobei $A_\alpha^+ $
				\end{itemize}
			\end{frame}
		\else
			Die Idee der TSVD\footnote{Truncated Singular Value Decomposition} ist es, die Konditionszahl, die in \cref{subsec:konditionszahl_allgemein} beschrieben wurde, möglichst klein zu halten, indem man alle Singulärwerte $\sigma_i$ -- beginnend bei $\sigma_r$ (der kleinsten) -- aus $\Sigma$ entfernt, welche zu einer zu großen Konditionszahl führen.
			
			Dabei entsteht aus dem $A$ das $A_\alpha$, welches wie folgt mit einem $\alpha > 0$ definiert wird (siehe ursprüngliche Definition von $A$ in \cref{subsec:singulaerwert_zerlegung}):
			\[ A_\alpha = U \Sigma_\alpha V^T  \]
			wobei
			\[ 
				\Sigma_\alpha = \left( 
				\begin{array}{*{3}{c}|*{3}{c}}
				\sigma_1 & & & & \vdots & \\
				& \ddots & & \cdots & 0 & \cdots \\
				& & \sigma_k & & \vdots & \\
				\cline{1-6}
				& \vdots & & & \vdots & \\
				\cdots & 0 & \cdots & \cdots & 0 & \cdots \\
				& \vdots & & & \vdots & \\
				\end{array}
				\right) 
			\]
			und
			\[ \frac{\sigma_1}{\sigma_k} \le \frac{1}{\alpha},\,\frac{\sigma_1}{\sigma_k+1} \ge \frac{1}{\alpha} \]
			
			Es muss im Programmcode bei einer numerischen Umsetzung, also im ersten Schritt, die Singulärwertzerlegung erfolgen, um im Anschluss die oben stehende Bedingung für diese zu prüfen und bei Nichtbestehen der Bedingung sie mit 0 zu ersetzen. 
			
			Dies wird im Struktogramm \cref{fig:struktogramm} verdeutlicht.
		\fi
		
			\begin{frame}{Struktogramm zur Singulärwert-Entfernung}
			
				\begin{figure}[H]
					
					\centering
					
					\caption[Struktogramm]{Struktogramm bezüglich des Entfernen von Singulärwerten aus $\Sigma$}
					
					\begin{struktogramm}(75,65)
						\assign{\( \alpha \gets 10^{-6} \)}
						\assign{\( \sigma \gets \diag{\left(\Sigma\right)} \)}
						\assign{\( i \gets 0 \)}
						\while[8]{\( i < \len{\left(\sigma\right)} \)}
							\assign{\( i \gets i+1 \)}
							\assign{\( \kappa =  \frac{\sigma\left[0\right]}{\sigma\left[i\right]} \)}
							\ifthenelse[14]{1}{1}
								{\( \sigma\left[i\right] > 0 \) und \( \kappa > \frac{1}{\alpha} \)}{\sTrue}{\sFalse}
								\assign[15]{\( \sigma\left[i\right] \gets 0 \)}
								\change
							\ifend
						\whileend
					\end{struktogramm}
					\label{fig:struktogramm}
					
				\end{figure}
			
			\end{frame}
		
		\ifpbeamer
		\else
			Gelöst wird das System im letzten Zuge mit dem Berechnen von $x_\alpha = A_\alpha^+ b$, wobei $A_\alpha^+$ die Pseudo-Inverse von $A_\alpha$ ist.
		\fi
		
	\section{Tikhonov-Regularisierung}
	\label{sec:tikhonov}
	
	
		\ifpbeamer
			\begin{frame}{Tikhonov}
				\begin{itemize}[<+->]
					\item Es wird für $\alpha > 0$ die Funktion
					$ J_\alpha(x) = \|Ax-b\|_2^2 + \alpha^2 \|x\|_2^2 $
					betrachtet
					\item Führt zu der Normalgleichung für $ \|Ax-b\|_2 \rightarrow min$, ergänzt um einen Term $ \alpha^2 I $
					\item Gewährleistet, dass die Systemmatrix $ A^T A + \alpha^2 I $ symmetrisch positiv definit ist
					\item Näherung ist dann $ x_\alpha = ( A^T A + \alpha^2 I )^{-1} A^T b $
					\item Tikhonov-Regularisierung konvergiert für $\alpha \rightarrow 0$ gegen die exakte Lösung $x$ (\cite{num-1})
				\end{itemize}
			\end{frame}
		\else
			Bei der Tikhonov-Regularisierung wird für $\alpha > 0$ die Funktion
			\[ J_\alpha(x) = \|Ax-b\|_2^2 + \alpha^2 \|x\|_2^2 \]
			betrachtet. 
			
			Diese wird, wie in \cite{num-1} beschrieben, umgeformt und führt zu der Normalgleichung für $ \|Ax-b\|_2 \rightarrow min$, ergänzt um einen Term $ \alpha^2 I $, welcher gewährleistet, dass die Systemmatrix $ A^T A + \alpha^2 I $ symmetrisch positiv definit\footnote{Symmetrisch positiv definite Matrizen sind symmetrisch und all ihre Untermatrizen sind positiv} ist:
			\[ ( A^T A + \alpha^2 I ) x_\alpha = A^T b \]
			
			Damit ist für $x = A^+ b$ zum Parameter $\alpha$ die Tikhonov-Regularisierung für $A \in \mathcal R^{m \times n}$, $\alpha > 0$:
			\[ x_\alpha = ( A^T A + \alpha^2 I )^{-1} A^T b \]
			
			Nach Satz 234 aus \cite{num-1} konvergiert die Tikhonov-Regularisierung für $\alpha \rightarrow 0$ gegen die exakte Lösung $x$.
		\fi
		
		
	\ifparticle
	
		%\end{multicols} % Artikel zeigt Code-Ausschnitt einspaltig an
	
	\fi
		
	\section{Programm}
	\label{sec:programm}
	
		\ifpbeamer
		\else
			Im Code-Ausschnitt \cref{fig:code} sieht man, wie die beiden Verfahren mit Hilfe der python-Bibliothek numpy umgesetzt werden.
			
			Diese Bibliothek wird wiefolgt in python eingebunden:
			
			\begin{verbatim}
				import numpy as np
			\end{verbatim}
		\fi
		
		\begin{frame}[fragile, allowframebreaks]{Python-Programm TSVD und Tikhonov}
			\begin{figure}[H]
		
				\caption[Code-Ausschnitt]{TSVD und Tikhonov in der Praxis}
				
				\lstinputlisting[stepnumber=5, firstnumber=30, linerange=40-100, breaklines=true]{../common/app.py}
				
				\label{fig:code}
				
			\end{figure}
		\end{frame}
	
		\ifpbook
		
			Den restlichen Code findet man in \cref{chap:sonstiges}.
			
			In \cref{fig:sequencediagram} ist ein Sequenzdiagramm zu sehen, das den Code aus \cref{fig:code} näher beschreibt.
			
		\fi
			
		\begin{frame}{Sequenzdiagramm}
			\begin{figure}[H]
				\begin{center}
					\caption[Sequenzdiagramm]{Sequenzdiagramm zum Programmiercode}
					
					\begin{sequencediagram}
						\newthread{t}{:Main}
						\newinst[1]{i}{:Numpy}
						
						\begin{call}{t}{linalg.svd(A)}{i}{}
						\end{call}
						\begin{call}{t}{linalg.pinv(A\_alpha)}{i}{}
						\end{call}
					\end{sequencediagram}
					
					\label{fig:sequencediagram}
				\end{center}
			\end{figure}
		\end{frame}
	
	\ifparticle
	
		%\begin{multicols}{2} % Artikel geht nach Code-Ausschnitt zweispaltig weiter
			
	\fi


	
	